import React from 'react';
import ReactDOM from 'react-dom/client';
import reportWebVitals from './reportWebVitals';
import { BrowserRouter } from 'react-router-dom';
import { Provider } from 'react-redux';

import store from './components/redux/redux-store';
import App from './App';
import './index.css';


//import store <Provider store={store}> </Provider>

ReactDOM.createRoot(document.getElementById('root'))
  .render(
    <BrowserRouter>
      <React.StrictMode>
        <Provider store={store}>
          <App />
        </Provider>
      </React.StrictMode>
    </BrowserRouter>
  );

reportWebVitals();
