
import style from './PlayerOfGame.module.css';

const PlayerOfGame = (props) => {
  return (
    <div className={style.best__players_box}>
      <div className={style.game__leader_header}>Game leaders</div>
      <div className={style.game__leader_txt} >
        <p>PLAYER </p>
        <p>pts/reb/ast</p>
      </div>
      <div className={style.bestplayers__items} >
        <div className={style.bestplayers__item}>
          <div>{props.playerHome.player}</div>
          <div>{props.playerHome.points}
            /{props.playerHome.rebounds}/{props.playerHome.assists}</div>
        </div>
        <div className={style.bestplayers__item}>
          <div>{props.playerGuest.player}</div>
          <div>{props.playerGuest.points}
            /{props.playerGuest.rebounds}/{props.playerGuest.assists}</div>
        </div>
      </div>
    </div>
  )
}

export default PlayerOfGame;