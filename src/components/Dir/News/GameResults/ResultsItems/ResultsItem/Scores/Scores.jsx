
import style from './Scores.module.css';

const Scores = (props) => {
  return (
    <div className={style.table__score}>
      <div className={style.teams__items}>
        <div className={style.teams__item}>
          <img className={style.teams__logo}
            src='https://cdn.nba.com/logos/nba/1610612738/global/L/logo.svg'
            alt='team logo' />
          <div className={style.team__name} >
            {props.teamHome.name}
          </div>
        </div>
        <div className={style.teams__item}>
          <img className={style.teams__logo}
            src='https://cdn.nba.com/logos/nba/1610612748/global/L/logo.svg'
            alt='team logo' />
          <div className={style.team__name} >
            {props.teamGuest.name}
          </div>
        </div>
      </div>
      <div className={style.results__score_line}>
        <div>{props.teamHome.totalPoints}</div>
        <div>{props.teamGuest.totalPoints}</div>
      </div>
      <button>boxscore</button>
    </div>
  )
}

export default Scores;