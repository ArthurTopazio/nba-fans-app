
import PlayerOfGame from './PlayerOfGame/PlayerOfGame';
import style from './ResultsItem.module.css';
import Scores from './Scores/Scores';

const ResultsItem = (props) => {
  let bestPlayerHome = props.teamHome.boxScore
    .sort((a, b) => { return b.points - a.points })[0];
  let bestPlayerGuest = props.teamGuest.boxScore
    .sort((a, b) => { return b.points - a.points })[0];
  return (
    <div className={style.results__box}>
      <Scores teamHome={props.teamHome} teamGuest={props.teamGuest} />
      <PlayerOfGame playerHome={bestPlayerHome}
        playerGuest={bestPlayerGuest} />
    </div>

  )
}

export default ResultsItem;