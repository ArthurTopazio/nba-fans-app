
import ResultsItem from './ResultsItem/ResultsItem';
import style from './ResultsItems.module.css';

const ResultsItems = (props) => {
  let ResultsItemElements = props.games.map(item => <ResultsItem
    key={item.gameNumber} teamHome={item.teamHome}
    teamGuest={item.teamGuest} />)
  return (
    <div className={style.results}>
      {ResultsItemElements}
    </div>
  )
}

export default ResultsItems;