import DateBox from './DateBox/DateBox';
import style from './GameResults.module.css';
import ResultsItems from './ResultsItems/ResultsItems';

const GameResults = (props) => {
  return (
    <div className={style.wrapper}>
      <DateBox date={props.date} toggleIsFetching={props.toggleIsFetching}
        isFetching={props.isFetching} />
      <ResultsItems games={props.games} />
    </div>
  )
}

export default GameResults;