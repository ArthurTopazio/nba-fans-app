import style from './DateBox.module.css';
import preloader from './../../../../../asserts/preload/preload2.gif';

/*let date = new Date();
let b = `${date.getDay()} ${date.getDate()} : ${date.getMonth()} : ${date.getFullYear()} `;
console.log(date.getDay()); */

const DateBox = (props) => {
  console.log(props)
  let load = () => {
    props.toggleIsFetching(true)
  };
  let unload = () => {
    props.toggleIsFetching(false)
  }
  return (
    <div className={style.date__box}>
      <div className={style.buttons}>
        <button className={style.button} onClick={load}>Prewius day</button>
        <button className={style.button} onClick={unload} >Next day</button>
      </div>
      {props.isFetching ? <img src={preloader} className={style.preloader} /> :
        <div className={style.date}>
          {props.date}
        </div>}
    </div>
  )
}

export default DateBox;