import { connect } from "react-redux";
import { toggleIsFetchingAC } from "../../../redux/gameResults-reducer";

import GameResults from "./GameResults";

let mapStateToProps = (state) => {
  return state.gameResults;
}

let mapDispatchToProps = (dispatch) => {
  return {
    toggleIsFetching: (isFetching) => {
      dispatch(toggleIsFetchingAC(isFetching));
    }
  }
}

const GameResultsContainer = connect(mapStateToProps, mapDispatchToProps)(GameResults);

export default GameResultsContainer;