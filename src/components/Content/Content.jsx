
import style from './Content.module.css';
import RoutesList from './RoutesList/RoutesList';

const Content = () => {
  return (
    <div className={style.main} >
      <RoutesList />
    </div>
  )
}

export default Content;