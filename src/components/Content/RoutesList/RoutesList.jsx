import {Routes, Route} from 'react-router-dom';

import HomePage from '../../Dir/HomePage/HomePage';
import HallOfFame from '../../Dir/HallOfFame/HallOfFame';
import News from '../../Dir/News/News';
import Shop from '../../Dir/Shop/Shop';
import Teams from '../../Dir/Teams/Teams';
import GameResultsContainer from
        '../../Dir/News/GameResults/GameResultsContainer';

const RoutesList = () => {
    return (
        <>
            <Routes>
                <Route path='' element={<HomePage/>}/>
            </Routes>
            <Routes>
                <Route path='/teams' element={<Teams/>}/>
                <Route path='/teams/East' element={<Teams/>}/>
                <Route path='/teams/West' element={<Teams/>}/>
                <Route path='/teams/All' element={<Teams/>}/>
            </Routes>
            <Routes>
                <Route path='/fame' element={<HallOfFame/>}/>
                <Route path='/fame/60' element={<HallOfFame/>}/>
                <Route path='/fame/70' element={<HallOfFame/>}/>
                <Route path='/fame/80' element={<HallOfFame/>}/>
                <Route path='/fame/90' element={<HallOfFame/>}/>
                <Route path='/fame/00' element={<HallOfFame/>}/>
            </Routes>
            <Routes>
                <Route path='/news' element={<News/>}/>
                <Route path='/news/all' element={<News/>}/>
                <Route path='/news/results' element={<GameResultsContainer/>}/>
                <Route path='/news/stats' element={<News/>}/>
                <Route path='/news/blogs' element={<News/>}/>
            </Routes>
            <Routes>
                <Route path='/jersey' element={<Shop/>}/>
                <Route path='/jersey/basic' element={<Shop/>}/>
                <Route path='/jersey/hwcl' element={<Shop/>}/>
                <Route path='/jersey/vice' element={<Shop/>}/>
                <Route path='/jersey/lmtd' element={<Shop/>}/>
                <Route path='/jersey/sg' element={<Shop/>}/>
                <Route path='/jersey/oth' element={<Shop/>}/>
            </Routes>
        </>
    )
};

export default RoutesList;

