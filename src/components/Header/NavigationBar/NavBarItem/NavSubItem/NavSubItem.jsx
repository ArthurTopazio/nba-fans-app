import { NavLink } from 'react-router-dom';

import style from './NavSubItem.module.css';

const navData = (link) => link.isActive ? style.active : style.item;

const NavSubItem = (props) => {
  return (
    <NavLink to={props.link}
      className={navData} >{props.case}</NavLink>
  )
}

export default NavSubItem;