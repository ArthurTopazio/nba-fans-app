import { NavLink } from 'react-router-dom';

import style from './NavBarItem.module.css';
import NavSubItem from './NavSubItem/NavSubItem';

const navData = (link) => link.isActive ? style.active : style.item;

const NavBarItem = (props) => {
  let subItemElements = props.items
    .map(item => <NavSubItem key={item.key} case={item.item}
      link={item.navLink} />);
  return (
    <div className={style.item}>
      <div className={style.head}>
        <NavLink to={props.case.caseLink}
          className={navData}>{props.case.caseName}</NavLink>
      </div>
      <div className={style.dropdown}>
        {subItemElements}
      </div>
    </div>
  )
}

export default NavBarItem;