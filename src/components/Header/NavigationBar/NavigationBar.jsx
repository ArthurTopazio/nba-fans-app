import style from './NavigationBar.module.css';
import NavBarItem from './NavBarItem/NavBarItem';

const NavigationBar = (props) => {
    let navBarElements = props.navBar
        .map(item => <NavBarItem key={item.id} case={item.navItem.case}
                                 items={item.navItem.items}/>)
    return (
        <div className={style.main}>
            {navBarElements}
        </div>
    )
}

export default NavigationBar;