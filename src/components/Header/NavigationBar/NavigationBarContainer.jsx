import { connect } from "react-redux";

import NavigationBar from "./NavigationBar";

let mapStateToProps = (state) => {
  return {
    navBar: state.navBar
  };
}

let NavigationBarContainer = connect(mapStateToProps)(NavigationBar);

export default NavigationBarContainer;