import UserProfile from './UserProfile/UserProfile';
import HeaderLogo from './HeaderLogo/HeaderLogo';
import NavigationBarContainer from './NavigationBar/NavigationBarContainer';

import style from './Header.module.css'


const Header = () => {
  return (
    <div className={style.main}>
      <div className={style.wrapper}>
        <HeaderLogo />
        <NavigationBarContainer />
        <UserProfile />
      </div>
    </div>
  )
}

export default Header;