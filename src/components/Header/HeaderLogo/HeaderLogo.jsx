import style from './HeaderLogo.module.css'

const HeaderLogo = () => {
  return (
    <div>
      <img src="" alt="Our Logo" />
    </div>
  )
}

export default HeaderLogo;