import { combineReducers, legacy_createStore } from "redux";
import React from 'react';

import navBarReducer from './navBar-reducer';
import gameResultsReducer from './gameResults-reducer';

let reducers = combineReducers({
  navBar: navBarReducer,
  gameResults: gameResultsReducer,
});

let store = legacy_createStore(reducers);

export default store;