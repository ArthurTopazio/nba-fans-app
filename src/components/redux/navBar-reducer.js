
let initialState = [
  {
    id: 1,
    navItem: {
      case: {
        caseName: 'Teams',
        caseLink: '/teams',
      },
      items: [
        { item: 'Eastern Conference', navLink: '/teams/East', key: 1, },
        { item: 'Western Conference', navLink: '/teams/West', key: 2, },
        { item: 'All', navLink: '/teams/All', key: 3, },
      ]
    }
  },
  {
    id: 2,
    navItem: {
      case: {
        caseName: 'Hall Of Fame',
        caseLink: '/fame',
      },
      items: [
        { item: '60-s', navLink: '/fame/60', key: 1, },
        { item: '70-s', navLink: '/fame/70', key: 2, },
        { item: '80-s', navLink: '/fame/80', key: 3, },
        { item: '90-s', navLink: '/fame/90', key: 4, },
        { item: '2000-s', navLink: '/fame/00', key: 5, },
      ]
    }
  },
  {
    id: 3,
    navItem: {
      case: {
        caseName: 'News/Results',
        caseLink: '/news',
      },
      items: [
        { item: 'All around', navLink: '/news/all', key: 1, },
        { item: 'Game Results', navLink: '/news/results', key: 2, },
        { item: 'Stats', navLink: '/news/stats', key: 3, },
        { item: 'Blogs', navLink: '/news/blogs', key: 4, },
      ]
    }
  },
  {
    id: 4,
    navItem: {
      case: {
        caseName: 'Jesrsey-shop',
        caseLink: '/jersey',
      },
      items: [
        { item: 'Basic jersey', navLink: '/jersey/basic', key: 1, },
        { item: 'Harwood Classic', navLink: '/jersey/hwcl', key: 2, },
        { item: 'Vice-edition', navLink: '/jersey/vice', key: 3, },
        { item: 'Limited', navLink: '/jersey/lmtd', key: 4, },
        { item: 'Signature', navLink: '/jersey/sg', key: 5, },
        { item: 'Other', navLink: '/jersey/oth', key: 6, },
      ]
    }
  },
];

const navBarReducer = (state = initialState, action) => {
  return state;
}

export default navBarReducer;