const TOGGLE_IS_FETCHING = 'TOGGLE_IS_FETCHING';

let initialState = {
  date: '03.06.2022',
  games: [
    {
      gameNumber: 1,
      teamHome: {
        name: 'CELTICS',
        totalPoints: 121,
        boxScore: [
          { player: 'Jason Tatum', points: 14, rebounds: 8, assists: 14 },
          { player: 'Al Horford', points: 25, rebounds: 14, assists: 4 },
        ]
      },
      teamGuest: {
        name: 'HEAT',
        totalPoints: 105,
        boxScore: [
          { player: 'Jimmy Butler', points: 44, rebounds: 12, assists: 8 },
          { player: 'Bem Adebayo', points: 13, rebounds: 15, assists: 3 },
        ]
      }
    },
    {
      gameNumber: 2,
      teamHome: {
        name: 'SIXERS',
        totalPoints: 114,
        boxScore: [
          { player: 'Joel Embid', points: 34, rebounds: 8, assists: 14 },
          { player: 'James Harden', points: 25, rebounds: 14, assists: 4 },
        ]
      },
      teamGuest: {
        name: 'LAKERS',
        totalPoints: 103,
        boxScore: [
          { player: 'LeBron James', points: 32, rebounds: 12, assists: 8 },
          { player: 'Russel Westbrook', points: 22, rebounds: 15, assists: 3 },
        ]
      }
    },
    {
      gameNumber: 3,
      teamHome: {
        name: 'DALLAS',
        totalPoints: 123,
        boxScore: [
          { player: 'Luka Doncic', points: 37, rebounds: 8, assists: 14 },
          { player: 'Jalen Branson', points: 27, rebounds: 14, assists: 4 },
        ]
      },
      teamGuest: {
        name: 'CAVS',
        totalPoints: 97,
        boxScore: [
          { player: 'Darius Garland', points: 34, rebounds: 12, assists: 8 },
          { player: 'Evan Mobley', points: 17, rebounds: 15, assists: 3 },
        ]
      }
    }
  ],
  isFetching: false,
}

let gameResultsReducer = (state = initialState, action) => {

  switch (action.type) {
    case TOGGLE_IS_FETCHING: {
      return {
        ...state,
        isFetching: action.isFetching,
      }
    }
  }
  return state;
};

export const toggleIsFetchingAC = (isFetching) =>
  ({ type: TOGGLE_IS_FETCHING, isFetching });


export default gameResultsReducer;